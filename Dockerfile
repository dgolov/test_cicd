FROM python:3.9.2

MAINTAINER Dmitriy Golov 'dgolov@icloud.com'

RUN apt-get update
ENV APP_HOME=/web
RUN mkdir -p $APP_HOME
COPY . $APP_HOME
WORKDIR $APP_HOME
RUN pip install --upgrade pip -r requirements.txt
EXPOSE 5000
